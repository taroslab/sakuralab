'**
'* 文字列をGoogleで検索する
'*
Option Explicit

'**
'* 文字列をGoogleで検索
'*
'* @return なし
'*
Function search_on_google()
	Dim shell_obj		' Shell.Application
	Dim slct_str		' 選択文字列
	Dim srch_str		' 検索文字列
	
	Set shell_obj = CreateObject("Shell.Application")
	
	' 選択文字列の取得
	slct_str = get_slct_str()
	
	' 検索文字列の作成
	srch_str = "http://www.google.co.jp/search?" & _
				"hl=ja&" & _
				"q=" & slct_str & "&" & _
				"lr=lang_ja&" & _
				"gws_rd=ssl"
	
	' アプリケーション実行
	shell_obj.ShellExecute(srch_str)
	
	Set shell_obj = Nothing
End Function

'**
'* 選択文字列を取得する。文字列が選択されていない場合、現在カーソルのある位置の
'* 単語を選択しそれを取得する。
'* 
'* @return 選択文字列
'*
Function get_slct_str()
	Dim ret_selt_str ' 選択文字列
	
	' 選択文字列の取得
	If IsTextSelected Then
		' 文字列が既に選択されている場合

		' 選択文字列を取得する
		ret_selt_str = GetSelectedString()
	Else
		' 文字列がまだ選択されていない場合

		' 現在カーソルのある位置の単語を選択する
		SelectWord()

		' 選択文字列を取得する
		ret_selt_str = GetSelectedString()
	End If

	get_slct_str = ret_selt_str
End Function

'******************************
'* Call Main Routine
'******************************
Call search_on_google()

