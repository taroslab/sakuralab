'**
'* 選択文字列をコメントアウトする
'*
Option Explicit

'**
'* 選択文字列をコメントアウトする。文字列を選択していない場合はコメントブロックを作成し、
'* その中央にカーソルを移動する。
'*
'* @return なし
'*
Function CommentOutInBlock()
	Dim slct_str	' 選択文字列
	Dim file_ext	' ファイル拡張子
	Dim cmnt_str	' コメントアウト後の文字列
	
	' 選択文字列取得
	slct_str = GetSelectedString()
	
	' ファイル拡張子取得
	file_ext = GetFilenameExtension()
	
	' コメントアウト後の文字列取得
	cmnt_str = GetCommentOutStr(slct_str, file_ext)
	
	If cmnt_str = "" Then
		Exit Function
	End If
	
	' コメントアウト
	' 選択文字列をコメントアウト後の文字列で上書きすることでコメントアウトする
	InsText(cmnt_str)
	
	' 文字列を選択していない場合、中央にカーソルを移動
	If slct_str = "" Then
		MoveCursor(file_ext)
	End If
End Function

'**
'* ファイル拡張子を取得する。
'*
'* @return ファイル拡張子
'*
Function GetFilenameExtension()
	Dim file_sys_obj	' ファイルシステムオブジェクト
	Dim file_path		' ファイルフルパス
	Dim file_ext		' ファイル拡張子
	
	' ファイルシステムオブジェクト作成
	Set file_sys_obj = CreateObject("Scripting.FileSystemObject")
	
	' ファイルフルパス取得
	file_path = ExpandParameter("$F")
	
	' ファイル拡張子取得
	file_ext = file_sys_obj.GetExtensionName(file_path)
	
	Set file_sys_obj = Nothing
	GetFilenameExtension = file_ext
End Function

'**
'* 文字列にコメントアウト用の文字列を結合し、その文字列を返す。コメントアウト用の文字列は
'* ファイル拡張子によって判別する。
'*
'* @param str 文字列
'* @param file_ext ファイル拡張子
'* @return コメントアウト後の文字列
'*
Function GetCommentOutStr(str, file_ext)
	Dim cout_str	' コメントアウト後の文字列
	
	Select Case file_ext
		Case "c", "cpp", "h"
			cout_str = "/* " & str & " */"
			
		Case "xml", "html"
			cout_str = "<!-- " & str & " -->"
			
		Case Else
			cout_str = ""
	End Select
	
	GetCommentOutStr = cout_str
End Function

'**
'* カーソルを移動する。ファイル拡張子によって移動距離を変える。
'*
'* @param file_ext ファイル拡張子
'* @return なし
'*
Function MoveCursor(file_ext)
	Dim i
	
	Select Case file_ext
		Case "c", "cpp", "h"
			For i = 1 To 3
				Editor.Left()
			Next
			
		Case "xml", "html"
			For i = 1 To 4
				Editor.Left()
			Next
			
		Case Else
			' DO NOTHING
	End Select
End Function

'******************************
'* Call Main Routine
'******************************
Call CommentOutInBlock()

