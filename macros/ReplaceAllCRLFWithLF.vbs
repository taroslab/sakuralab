'**
'*  改行コードCRLFをすべてLFで置換
'*
Option Explicit

'**
'* 改行コードCRLF(\r\n)をすべてLF(\n)で置換する。
'*
'* @return なし
'*
Function replace_all_crlf_with_lf()
	Const new_line_code = 3	' サクラエディタ上の改行コードLFを表す番号
	
	' CRLF(\r\n)をすべてLF(\n)で置換
	' 62(10進)=00 0011 1110(2進)に関してはサクラエディタのS_ReplaceAllのヘルプを参照
	' 具体的には以下の条件で検索した。
	' - 英大文字と小文字を区別する
	' - 正規表現を使用
	' - 見つからないときにメッセージを表示
	' - 置換ダイアログを自動的に閉じる
	' - 先頭（末尾）から再検索する 
	' - ファイル全体を検索
	ReplaceAll "\r\n", "\n", 62
	
	' エディタの改行コードを指定
	ChgmodEOL new_line_code
	
	' 検索マークのクリア
	SearchClearMark
	
	' 画面の再描画
	ReDraw
End Function

'******************************
'* Call Main Routine
'******************************
Call replace_all_crlf_with_lf()
